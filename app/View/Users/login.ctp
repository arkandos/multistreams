<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Login"); ?>
<div class="container">
	<div class="page-header">
		<h1>Login</h1>
	</div>
	
	<?php echo $this->Form->create("User", array(
		"inputDefaults" => array(
			"div" => "form-group",
			"class" => "form-control input-lg"
	))); ?>
		
		<?php echo $this->Form->input("username"); ?>
		<?php echo $this->Form->input("password"); ?>
		
	<?php echo $this->Form->end(array(
		"label" => "Login",
		"div" => false,
		"class" => "btn btn-primary btn-lg"
	)); ?>
	
</div>
