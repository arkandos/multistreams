<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Confirm Requests"); ?>
<div class="container">
	<div class="page-header">
		<h1>Confirm Requests</h1>
	</div>

	<div class="panel panel-default">
		<?php echo $this->Html->tag("div", $request["Summoner"]["name"] . " for " . $request["Summoner"]["channel_name"], array("class" => "panel-heading")); ?>
		<div class="panel-body">

			<div class="container-fluid">

				<h3>Summoner Info</h3>

				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Summoner Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $this->Html->link($request["Summoner"]["name"], "http://www.lolking.net/summoner/" . $request["Summoner"]["region"] . "/" . $request["Summoner"]["id"]); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Region:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $request["Summoner"]["region"]; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Number of Requests:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $request["Summoner"]["requests"]; ?>
					</div>
				</div>

				<h3>Channel Info</h3>

				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Channel Name:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $this->Html->link($request["Channel"]["display_name"], $request["Channel"]["url"]); ?>
						<?php if($request["Channel"]["is_live"]) echo "( Live )"; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Followers:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $request["Channel"]["followers_count"]; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<strong>Last known Viewers Count:</strong>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8">
						<?php echo $request["Channel"]["viewers_count"]; ?>
					</div>
				</div>

				<h3></h3>


			</div>

			<div class="clearfix">
				<div class="pull-right">
					<?php
						echo $this->Form->create("Summoner", array(
							"inputDefaults" => array(
								"div" => false,
								"label" => false,
								"id" => false,
						)));

						echo $this->Form->input("id", array("type" => "hidden"));
						echo $this->Form->input("confirmed", array("type" => "hidden", "default" => 1));

						 echo $this->Form->end(array(
							"label" => "Confirm Summoner",
							"div" => false,
							"class" => "btn btn-success btn-lg",
						));
					?>
				</div>
				<div class="pull-left">
					<?php
						echo $this->Form->create("Summoner", array(
							"inputDefaults" => array(
								"div" => false,
								"label" => false,
								"id" => false,
						)));

						echo $this->Form->input("id", array("type" => "hidden"));
						echo $this->Form->input("confirmed", array("type" => "hidden", "default" => 0));

						 echo $this->Form->end(array(
							"label" => "Decline Summoner",
							"div" => false,
							"class" => "btn btn-danger btn-lg",
						));
					?>
				</div>
			</div>

		</div>
	</div>
</div>
