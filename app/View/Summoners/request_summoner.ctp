<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Request Summoner"); ?>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<h1>Request Summoner</h1>

			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#SummonerByName" role="tab" data-toggle="tab">By Region &amp; Summoner Name</a></li>
				<li role="presentation"><a href="#SummonerByLolking" role="tab" data-toggle="tab">By Lolking Url</a></li>
			</ul>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="SummonerByName">
					<div class="panel-body">
						<p>
							Request to display summoner info below a stream.<br />
							The requested summoner has to be confirmed by a moderator first before it will be visible.
						</p>

						<div class="form-group">
							<label class="control-label" for="SummonerChannelName">Channel Name:</label>
							<input class="form-control input-lg" id="SummonerChannelName" type="text" data-bind="value: channelName, valueUpdate: 'input'" />
						</div>
						<div class="form-group">
							<label class="control-label" for="SummonerRegion">Region:</label>
							<select class="form-control input-lg" id="SummonerRegion" data-bind="options: regions, optionsText: 'title', value: region">
							</select>
						</div>
						<div class="form-group">
							<label class="control-label" for="SummonerName">Summoner Name:</label>
							<input class="form-control input-lg" id="SummonerName" type="text" data-bind="value: summonerName, valueUpdate: 'input'">
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="SummonerByLolking">
					<div class="panel-body">
						<p>
							Request to display summoner info below a stream.<br />
							The requested summoner has to be confirmed by a moderator first before it will be visible.
						</p>
						<div class="form-group">
							<label class="control-label" for="SummonerChannelName">Channel Name:</label>
							<input class="form-control input-lg" id="SummonerChannelName" type="text" data-bind="value: channelName" />
						</div>
						<div class="form-group" data-bind="css: { 'has-success': lolkingState() == 'success', 'has-error': lolkingState() == 'error' }">
							<label class="control-label" for="SummonerLolking">Paste Lolking Url:</label>
							<input class="form-control input-lg" id="SummonerLolking" type="text" data-bind="value: summonerLolking, valueUpdate: 'input'">
						</div>
					</div>
				</div>
			</div>

			<?php echo $this->Form->create("Summoner", array(
				"inputDefaults" => array(
					"div" => false,
					"label" => false,
					"id" => false,
			))); ?>

			<div style="display:none;">
				<?php echo $this->Form->input("Summoner.channel_name", array("data-bind" => "value: channelName", "type" => "hidden")); ?>
				<?php echo $this->Form->input("Summoner.region", array("data-bind" => "value: region().region", "type" => "hidden")); ?>
				<?php echo $this->Form->input("Summoner.id", array( "data-bind" => "value: summonerId", "type" => "hidden")); ?>
				<?php echo $this->Form->input("Summoner.name", array("data-bind" => "value: summonerName", "type" => "hidden")); ?>
			</div>

			<?php echo $this->Form->end(array(
				"label" => "Request Summoner",
				"div" => false,
				"class" => "btn btn-primary btn-lg",
				"data-bind" => "css: { disabled: hasError }"
			)); ?>
		</div>
	</div>


</div>
<?php echo $this->Html->script("app/summoners-request.js"); ?>
