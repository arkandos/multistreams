<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;FAQ"); ?>
<div class="container">
	<div class="page-header">
		<h1>Frequently asked Questions</h1>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-3">
			<div id="faq-sidebar">
				<ul class="nav nav-pills nav-stacked" role="tablist">
					<li><a href="#faqWhatIsThis">What is this?</a></li>
					<li><a href="#faqWhatIsLive">Why does this exist?</a></li>
					<li><a href="#faqWhereIsMyStream">Why can't I find my favorite streamer?</a></li>
					<li><a href="#faqRelativeSorting">What are "Relative Viewers"?</a></li>
					<li><a href="#faqLoading">Why does it sometimes take longer to load?</a></li>
					<li><a href="#faqThisIsUgly">Why does it look so much like Bootstrap?</a></li>
				</ul>
			</div>
		</div>
		<div class="col-xs-12 col-sm-9">
			<div id="faqWhatIsThis" class="panel panel-default">
				<div class="panel-body">
					<h3>What is this?</h3>
					<p>
						This website lets you browse and watch all of your favorite League of Legends streamers, regardless of where they stream.
					</p>
					<p>
						Currently supported are streams from Twitch, Azubu and Hitbox.
					</p>
				</div>
			</div>
			<div id="faqWhatIsLive" class="panel panel-default">
				<div class="panel-body">
					<h3>Why does this exist?</h3>
					<p>
						Many pro players stream on sites other than twitch, but get way less viewers than they would get on twitch.
						Even players like Faker or xPeke rarely get over 10,000 viewers on Azubu.
					</p>
					<p>
						We try to improve this by ignoring total viewer numbers (you can still sort the channels by their viewers) and creating
						one interface where you can watch and follow channels regardless of where they stream.
					</p>
				</div>
			</div>
			<div id="faqWhereIsMyStream" class="panel panel-default">
				<div class="panel-body">
					<h3>Why can't I find my favorite streamer?</h3>
					<p>
						Make sure the channel you look for is online, we only ever display online channels.
					</p>
					<p>
						We always display all live channels from Azubu and Hitbox, but we only get a list of the first 100 streams from Twitch,
						so if your streamer has not enough viewers to be under the top 100 on twitch, we won't get his information.
					</p>
				</div>
			</div>
			<div id="faqRelativeSorting" class="panel panel-default">
				<div class="panel-body">
					<h3>What are "Relative Viewers"?</h3>
					<p>
						<strong>Sorting by relative viewers means the size of the streaming platform is ignored.</strong><br />
						This is the default option on the "Live Channels" page; you can change the sorting method by clicking on the "Sort By" button right of the page title.
						All other lists of streams are sorted by their total viewer count by default.
					</p>
					<p>
						For that, we divide the viewer count of a channel by the total number of people watching on this streaming platform.
						For example, if Twitch has a total of 100,000 people watching and Azubu has a total of 1,000, a Twitch stream with 10,000 viewers would have the same score as a Azubu stream with 100 viewers.
					</p>
					<p>
						This is done to equalize the viewer numbers and promote smaller channels that just happen to be small because of their preferred streaming platform.
					</p>
				</div>
			</div>
			<div id="faqLoading" class="panel panel-default">
				<div class="panel-body">
					<h3>Why does it sometimes take longer to load?</h3>
					<p>
						All pages use a 2-step loading model, ensuring your browser can render the page as fast as possible for you.<br />
						On the second step, the server can decide to update the database, which causes it to take a few seconds until the new data is available.
					</p>
				</div>
			</div>
			<div id="faqThisIsUgly" class="panel panel-default">
				<div class="panel-body">
					<h3>Why does it look so much like Bootstrap?</h3>
					<p>
						<strong>This website is currently in a alpha status and gets constantly changed</strong>
					</p>
					<p>
						I wanted to first get the technical side of things done, so I'm just using the default theme until everything is working.<br />
						Currently, I'm working on sorting streams by their solo queue rank and also displaying runes/masteries used in the current game directly under the stream.
					</p>
					<p>
						This is built on <a href="http://jquery.com" _target="blank">jQuery</a>, <a href="http://getbootstrap.com/" _target="blank">Bootstrap</a> and <a href="http://knockoutjs.com/" _target="blank">Knockout.js</a>.<br />
						On the server site I use <a href="http://cakephp.org/" _target="blank">CakePHP</a> for all the routing and database access.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>
<?php echo $this->Html->script("app/pages-faq.js"); ?>
