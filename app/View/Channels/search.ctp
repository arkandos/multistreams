<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Search for Channels"); ?>
<div class="container">
	<div class="page-header">
		<h1>Search Channels</h1>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			
			<div class="input-group input-group-lg">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-search"></span>
				</span>
				<input type="text" class="form-control" placeholder="Search Channels..." data-bind="value: searchTerm, valueUpdate: 'afterkeydown', submit: update" />
			</div>
			
		</div>
	</div>
	
	<div class="row top-buffer" data-bind="visible: !isLoading() && throttledSearchTerm().length > 0 && searchResults().length == 0" style="display: none">
		<div class="col-xs-12">
			<div class="alert alert-info" role="alert"><center>
				<strong>No channels found :(</strong><br />
				Maybe the Streamer you search isn't live or didn't make it into our data yet.
			</center></div>
		</div>
	</div>
	
	<div data-bind="visible: isLoading" style="display: none">
		<div class="loader">Loading...</div>
	</div>
	
	<channel-list params="channels: searchResults"></channel-list>
	
</div>

<?php echo $this->Html->script("app/channels-search.js"); ?>
