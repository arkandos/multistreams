<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Watch " . $channel["Channel"]["display_name"]); ?>
<div class="container-fluid revert-nav-spacing">
	<div data-bind="visible: isLoading">
		<div class="loader">Loading...</div>
	</div>

	<!-- ko if: !isLoading() && channel().hasOwnProperty('Channel') -->
	<!-- ko with: channel -->

	<div class="row">
		<div class="col-xs-9 col-no-padding">
			<div class="embed-responsive embed-responsive-15by9">
				<iframe id="video-embed" allowfullscreen="true" data-bind="attr: { src: Channel.embed_video_url }"></iframe>
			</div>
		</div>
		<div class="col-xs-3 col-no-padding">
			<iframe id="chat-embed" data-bind="attr: { src: Channel.embed_chat_url }, event: { scroll: $parent.preventBubble, mousewheel: $parent.preventBubble, DOMMouseScroll: $parent.preventBubble  }"></iframe>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-9">
			<div class="page-header">
				<h1 data-bind="text: Channel.title"></h1>
				<div class="clearfix">
					<p class="pull-left">
						<button type="button" class="btn" data-bind="click: $root.toggleFavorite,
							text: $root.nextToggleEventText,
							css: { 'btn-default': $root.isFavorite(), 'btn-primary': !$root.isFavorite() }">
						</button>
						<a class="btn btn-default" data-bind="attr: { href: Channel.url}">Go to Channel</a>
					</p>
					<ul class="nav nav-pills pull-right">
						<li class="active"><a href="#tabChannelDescription" role="tab" data-toggle="tab">Description</a></li>
						<!-- ko if: $root.summoners().length > 0 -->
						<li><a href="#tabMasteries" role="tab" data-toggle="tab" data-bind="click: $root.updateSummonerInfo">Masteries</a></li>
						<li><a href="#tabRunes" role="tab" data-toggle="tab" data-bind="click: $root.updateSummonerInfo">Runes</a></li>
						<!-- /ko -->
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- /ko -->

	<div class="row">
		<div class="col-xs-9 tab-content">
			<div class="tab-pane active" id="tabChannelDescription">
				<div id="channel-description" data-bind="html: channel().Channel.description"></div>
			</div>
			<div class="tab-pane" id="tabMasteries">
				<div class="container-fluid">
					<div class="col-xs-3">
						<ul class="nav nav-pills nav-stacked" data-bind="foreach: summoners">
							<li data-bind="css: { active: $root.currentSummoner() != null && Summoner.id == $root.currentSummoner().Summoner.id }">
								<a href="#" data-bind="click: function() { $root.currentSummonerId($index()); }">
									<span data-bind="text: Summoner.name"></span>
									[<span data-bind="text: Summoner.region"></span>]
								</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-9" data-bind="if: currentSummoner() != null">
						<h3>
							<strong data-bind="text: currentSummoner().Summoner.name"></strong> -
							<span data-bind="text: tierNames[ currentSummoner().Summoner.tier_number ]"></span>
							<span data-bind="text: divisionNames[ currentSummoner().Summoner.division ]"></span> -
							<span data-bind="text: currentSummoner().Summoner.league_points"></span> LP -
							<a target="_blank" data-bind="attr: { href: 'http://www.lolking.net/summoner/'+currentSummoner().Summoner.region+'/'+currentSummoner().Summoner.id }">LolKing</a>
						</h3>
						<masteries params="masteries: currentSummoner().Summoner.current_masteries"></masteries>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tabRunes">
				<div class="container-fluid">
					<div class="col-xs-3">
						<ul class="nav nav-pills nav-stacked" data-bind="foreach: summoners">
							<li data-bind="css: { active: $root.currentSummoner() != null && Summoner.id == $root.currentSummoner().Summoner.id }">
								<a href="#" data-bind="click: function() { $root.currentSummonerId($index()); }">
									<span data-bind="text: Summoner.name"></span>
									[<span data-bind="text: Summoner.region"></span>]
								</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-9" data-bind="if: currentSummoner() != null">
						<h3>
							<strong data-bind="text: currentSummoner().Summoner.name"></strong> -
							<span data-bind="text: tierNames[ currentSummoner().Summoner.tier_number ]"></span>
							<span data-bind="text: divisionNames[ currentSummoner().Summoner.division ]"></span> -
							<span data-bind="text: currentSummoner().Summoner.league_points"></span> LP -
							<a target="_blank" data-bind="attr: { href: 'http://www.lolking.net/summoner/'+currentSummoner().Summoner.region+'/'+currentSummoner().Summoner.id }">LolKing</a>
						</h3>
						<runes params="runes: currentSummoner().Summoner.current_runes"></runes>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- /ko -->
</div>

<?php echo $this->Html->script("app/channels-watch.js"); ?>
