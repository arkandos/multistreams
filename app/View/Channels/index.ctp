<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;league-streams.com"); ?>
<div class="container-fluid">
	<div class="jumbotron">
		<h1>All your favorite streamers, on one site</h1>
		<p>
			This is <?php echo $this->Html->link("league-streams.com", "/"); ?>, a place where you can see all your favorite <a href="http://leagueoflegends.com/">League of Legends</a> streamers, regardless of which streaming service they use.
		</p>

		<div class="container">
			<div id="topChannelsCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
				<ol class="carousel-indicators">

					<?php
						foreach($channels as $i => $channel) {
							echo $this->Html->tag("li", "", array(
								"data-target" => "#topChannelsCarousel",
								"data-slide-to" => $i,
								"class" => (($i == 0) ? "active" : "")
							));
						}
					?>

				</ol>
				<div class="carousel-inner" role="listbox">
					<?php
						foreach($channels as $i => $channel) {
							echo $this->Html->tag("div",
								$this->Html->link(
									$this->Html->image($channel["Channel"]["preview_url"], array("width" => "100%"))
									, array("controller" => "channels", "action" => "watch", $channel["Channel"]["name"]), array("escape" => false))
								. $this->Html->tag("div",
									$this->Html->tag("h3", $channel["Platform"]["display_name"] . " - " . $channel["Channel"]["display_name"])
									. $this->Html->tag("p", $channel["Channel"]["title"])
									, array("class" => "carousel-caption"))
								,array("class" => "item " . (($i == 0) ? "active" : "")));
						}
					?>
				</div>

				<a class="left carousel-control" href="#topChannelsCarousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#topChannelsCarousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</div>
