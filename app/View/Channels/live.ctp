<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Live Channels"); ?>
<div class="container">

	<div class="page-header">
		<h1>Live Channels

			<div class="dropdown btn-group pull-right">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					Sort By:
					<span data-bind="text: currentSort().name"></span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" data-bind="foreach: sortOptions">
					<li><a href="#" data-bind="click: $root.currentSort, text: name"></a></li>
				</ul>
			</div>
		</h1>
	</div>

	<channel-list params="channels: channels"></channel-list>

	<div data-bind="visible: channels().length == 0">
		<div class="loader">Loading...</div>
	</div>

	<div class="row clearfix">
		<ul class="pager">
			<li class="previous" data-bind="css: { disabled: currentPage() == 0 }">
				<a href="#" data-bind="click: previousPage">&larr; Previous</a>
			</li>
			<li class="next" data-bind="css: { disabled: currentPage() + 1 == pagesCount() }">
				<a href="#" data-bind="click: nextPage">&rarr; Next</a>
			</li>
		</ul>
	</div>
</div>

<?php echo $this->Html->script("app/channels-live.js"); ?>
