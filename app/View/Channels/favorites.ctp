<?php $this->assign("title", "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;Favorite Channels"); ?>
<div class="container">
	<div class="page-header">
		<h1><span class="glyphicon glyphicon-star"></span> Favorite Channels</h1>
	</div>
	
	<div data-bind="visible: isLoading" style="display: none">
		<div class="loader">Loading...</div>
	</div>
	
	<div class="row top-buffer" data-bind="visible: (!isLoading() && favoriteChannels().length == 0 && favorites().length > 0)" style="display: none">
		<div class="col-xs-12">
			<div class="alert alert-info" role="alert"><center>
				<strong>None of  your favorite channels are currently online :(</strong><br />
				Check out
				<?php echo $this->Html->link("Live Channels", array("controller" => "channels", "action" => "index")); ?>
				to see a complete list or
				<?php echo $this->Html->link("Search", array("controller" => "channels", "action" => "search")); ?>
				for a channel.
			</center></div>
		</div>
	</div>
	
	<div class="row top-buffer" data-bind="visible: favorites().length == 0" style="display: none;">
		<div class="col-xs-12">
			<div class="alert alert-info" role="alert"><center>
				<strong>You didn't favorite any channels yet</strong><br>
				To add a channel to your favorites, click on the little star next to their name and viewer count.<br />
				Favorites are stored
				<a href="http://wikipedia.org/wiki/Web_Storage" target="_blank">in your browser</a>,
				without requiring any registration.
			</center></div>
		</div>
	</div>
	
	<channel-list params="channels: favoriteChannels"></channel-list>
	
	<div class="row">
		<div class="col-xs-12">
			<button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#importTwitchDialog">Import from Twitch</button>
			<button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#removeAllDialog" data-bind="css: { disabled: favorites().length == 0 }">Delete All Favorites</button>
		
			<div class="modal fade" id="importTwitchDialog" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Import Twitch Followers</h4>
						</div>
						<div class="modal-body">
							<p>
								Note that even though all your followed channels will be imported,
								you can only see the ones in our database.<br />
								<strong>Enter your Twitch username:</strong>
							</p>
							<p>
								<input class="form-control" placeholder="Twitch username" data-bind="value: importUsername"/>
							</p>
							<div role="alert" data-bind="text: importStatus, visible: importStatus().length > 0, attr: { class: 'alert alert-'+importStatusType() }"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary" data-bind="click: importFavorites">Import!</button>
						</div>
					</div>
				</div>
			</div>
		
			<div class="modal fade" id="removeAllDialog" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Are you sure?</h4>
						</div>
						<div class="modal-body">
							<p>
								Are you sure you want to delete all your favorites forever?<br />
								This will delete <strong>all <span data-bind="text: favorites().length"></span> of them</strong>, even those not listed because they are offline.
							</p>
							<p>
								<span data-bind="text: favorites().join(', ')"></span>
							<p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" data-bind="click: clearFavorites">Yes, delete them!</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">Better don't do that!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $this->Html->script("app/channels-favorites.js"); ?>
