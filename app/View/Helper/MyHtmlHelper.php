<?php

App::uses("HtmlHelper", "View/Helper");
class MyHtmlHelper extends HtmlHelper {

	//construct a link of list complete with "active" classes and stuff from a
	//(controller, action) array. the link title is the title member of that array.
	public function linkList($links, $activeClass = "active", $containerOptions = array(), $wrapperOptions = array(), $linkOptions = array()) {
	
		$containerTag = "ul";
		if(isset($containerOptions["tag"])) {
			$containerTag = $containerOptions["tag"];
			unset($containerOptions["tag"]);
		}
		$wrapperTag = "li";
		if(isset($wrapperOptions["tag"])) {
			$wrapperTag = $wrapperOptions["tag"];
			unset($wrapperOptions["tag"]);
		}
		
		if(!$wrapperTag) {
			$wrapperOptions = $linkOptions;
		}
	
		$listHtml = "";
		foreach($links as $link) {
			$linkTitle = $link["title"];
			unset($link["title"]);
		
			$options = $wrapperOptions;
			if($this->request->controller == $link["controller"]
				&& (isset($link["matchController"])
					|| !isset($link["action"])
					|| $this->request->action ==$link["action"]))
			{
				if(!isset($options["class"])) $options["class"] = "";
				$options["class"] .= " ".$activeClass;
				unset($link["matchController"]);
			}
			if(!$wrapperTag) {
				$listHtml .= $this->link($linkTitle, $link, $options);
			} else {
				$listHtml .= $this->tag($wrapperTag,
						$this->link($linkTitle, $link, $linkOptions),
					$options);
			}
		}
		
		if(!$containerTag) {
			return $listHtml;
		} else {
			return $this->tag($containerTag, $listHtml, $containerOptions);
		}
	}
}

?>