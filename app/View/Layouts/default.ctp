<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset("utf-8"); ?>
	<title><?php echo $this->fetch("title"); ?></title>

	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min.css');
		// echo $this->Html->css('bootstrap-theme.min.css');
		echo $this->Html->css('multistreams.css');

		echo $this->Html->script("require.js");

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<script>
		require.config({
			baseUrl:  "/js/",
			deps: ["jquery", "bootstrap"],
			shim: {
				"bootstrap": { deps: ["jquery"] },
				"knockout-bootstrap": { deps: ["bootstrap", "knockout"] }
			},
			waitSeconds: 30,
		});
	</script>
</head>
<body>
	<div id="navbar" class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<?php echo $this->Html->link("[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]&nbsp;&nbsp;league-streams Alpha!", "/", array("class" => "navbar-brand", "escapeTitle" => false)); ?>
			</div>
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">


				<?php
				$links = array(
					array("title" => "Live Channels", "controller" => "channels", "action" => "live"),
					array("title" => "Favorites", "controller" => "channels", "action" => "favorites"),
					array("title" => "Search", "controller" => "channels", "action" => "search"),
					array("title" => "About", "controller" => "pages", "action" => "display", "faq")
				);
				if($this->request->controller == "channels" && $this->request->action == "watch") {
					array_unshift($links, array("title" => $channel["Channel"]["display_name"], "controller" => "channels", "action" => "watch", $channel["Channel"]["name"]));
				}
				echo $this->Html->linkList($links, "active", array("class" => "nav navbar-nav")); ?>
			</div>
		</div>
	</div>


	<div class="content">
		<div class="container">
			<?php echo $this->Session->flash("flash", array("params" => array("class" => "alert alert-info"))); ?>
		</div>

		<?php echo $this->fetch("content"); ?>
	</div>

	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<h4>Channels</h4>
					<ul>
						<li><?php echo $this->Html->link("Live Channels", array("controller" => "channels", "action" => "live")); ?></li>
						<li><?php echo $this->Html->link("Favorite Channels", array("controller" => "channels", "action" => "favorites")); ?></li>
						<li><?php echo $this->Html->link("Search Channels", array("controller" => "channels", "action" => "search")); ?></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4">
					<h4>Summoners</h4>
					<ul>
						<li><?php echo $this->Html->link("Request Summoner", array("controller" => "summoners", "action" => "request_summoner")); ?></li>
						<?php if($loggedIn): ?>
							<li><?php echo $this->Html->link("Confirm Requests", array("controller" => "summoners", "action" => "confirm_requests")); ?></li>
							<li><?php echo $this->Html->link("Add Summoner", array("controller" => "summoners", "action" => "add")); ?></li>
						<?php endif; ?>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4">
					<h4>About</h4>
					<ul>
						<li><?php echo $this->Html->link("FAQ", array("controller" => "pages", "action" => "display", "faq")); ?></li>
						<?php if(!$loggedIn) echo $this->Html->tag("li", $this->Html->link("Login", array("controller" => "users", "action" => "login"))); ?>
						<?php if($loggedIn): ?>
							<?php if($user["role"] == "admin") echo $this->Html->tag("li", $this->Html->link("Add User", array("controller" => "users", "action" => "add"))); ?>
							<li><?php echo $this->Html->link("Logout", array("controller" => "users", "action" => "logout")); ?></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
