//i like this hack
require(["jquery", "domReady!"], function($) {
	var user = window.location.pathname.match(/^\/watch\/([^\/]+)/)[1];
	if(user) $.ajax({
		url: "https://api.twitch.tv/channels/"+user+"/panels",
		dataType: "jsonp",
		success: function(response) {
			//just to make sure; it should be in the right order already
			response.sort(function(a, b) { return a.display_order - b.display_order; });

			var descriptionHtml = "<div class='container-fluid'><div class='hidden'>";
			for(var i = 0; i < response.length; ++i) {
				if(i % 3 == 0) descriptionHtml += "</div><div class='row'>";
				descriptionHtml += "<div class='col-xs-4'>";

				if(response[i].data.title !== undefined && response[i].data.title.length > 0) {
					descriptionHtml += "<h3>" + response[i].data.title + "</h3>";
				}
				if(response[i].data.image !== undefined && response[i].data.image.length > 0) {
					var imageHtml = "<img class='img-responsive' src='"+response[i].data.image+"' />";
					if(response[i].data.link !== undefined && response[i].data.link.length > 0) {
						imageHtml = "<a href='"+response[i].data.link+"' target='_blank'>"+imageHtml+"</a>";
					}
					descriptionHtml += imageHtml;
				}

				if(response[i].html_description) {
					descriptionHtml += response[i].html_description;
				}

				descriptionHtml += "</div>";
			}
			descriptionHtml += "</div></div>";

			$("#channel-description").html(descriptionHtml);
		}
	});
});
