require(["jquery", "knockout", "knockout.localStorage", "domReady!"], function($, ko) {

	ko.components.register("channel-list", { require: "components/channel-list" });

	function LiveChannelsViewModel() {
		var self = this;

		self.channels = ko.observableArray();

		self.pagesCount = ko.observable(0);
		self.currentPage = ko.observable(0);
		self.firstPage = function() {
			self.currentPage(0);
		};
		self.nextPage = function() {
			var current = self.currentPage(), count = self.pagesCount();
			if(current + 1 < count) self.currentPage(current+1);
		};
		self.previousPage = function() {
			var current = self.currentPage();
			if(current > 0) self.currentPage(current-1);
		};
		self.currentPage.subscribe(function() {
			self.update();
		});

		self.sortOptions = [
			{name: "Relative Viewers", sort: "relative"},
			{name: "Total Viewers", sort: "viewers"},
			{name: "Highest Rating", sort: "elo"},
			{name: "Number of Followers", sort: "followers"},
		];
		self.currentSort = ko.observable(self.sortOptions[0]).extend({ localStorage: "channelsSort" });
		self.currentSort.subscribe(function() {
			var currentPage = self.currentPage();
			// changing the current page forces an update anyways
			if(currentPage != 0) self.currentPage(0);
			else self.update();
		});

		self.update = function() {
			self.channels([]);
			$.ajax({
				url: "/channels/live/sort:"+self.currentSort().sort+"/page:"+self.currentPage() + "/.json",
				dataType: "json",
				success: self.onUpdateSuccess
			});
		};
		self.onUpdateSuccess = function(response) {
			self.channels(response["channels"]);
			self.pagesCount(response["pages_count"]);
		};

		//do the first update automatically to have some data at least
		self.update();
	}

	ko.applyBindings(new LiveChannelsViewModel());

});
