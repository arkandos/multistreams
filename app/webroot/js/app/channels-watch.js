require(["jquery", "knockout", "knockout.localStorage", "domReady!"], function($, ko) {

	ko.components.register("masteries", { require: "components/masteries" });
	ko.components.register("runes", { require: "components/runes" });

	function ChannelsViewModel() {
		var self = this;

		self.channel = ko.observable({});
		self.favorites = ko.observableArray([]).extend({ localStorage: "favoriteChannels" });
		self.isFavorite = ko.pureComputed(function() {
			return (self.channel().Channel && self.favorites.indexOf(self.channel().Channel.name) >= 0);
		}, self);
		self.nextToggleEventText = ko.pureComputed(function() {
			return self.isFavorite() ? "Remove Favorite" : "Add Favorite";
		}, self);
		self.toggleFavorite = function() {
			if(self.isFavorite()) self.favorites.remove(self.channel().Channel.name);
			else self.favorites.push(self.channel().Channel.name);
		};

		self.isLoading = ko.observable(false);
		self.update = function(callback) {
			self.isLoading(true);
			$.ajax({
				url: location.pathname + ".json",
				dataType: "json",
				success: function(response) {
					self.channel(response.channel);
					self.windowWidth.valueHasMutated();
					self.windowHeight.valueHasMutated();

					if(callback !== undefined) callback();
				},
				complete: function() {
					self.isLoading(false);
				}
			});
		};

		self.summoners = ko.observableArray(null);
		self.tierNames = [ "UNRANKED", "Bronze", "Silver", "Gold", "Platinum", "Diamond", "Master", "Challenger" ];
		self.divisionNames = [ "??", "I", "II", "III", "IV", "V" ];

		self.currentSummonerId = ko.observable(0);
		self.currentSummoner = ko.pureComputed(function() {
			var summoners = self.summoners(),
				currentId = self.currentSummonerId();
			return (currentId >= 0 && currentId < summoners.length) ? summoners[currentId] : null;
		}, self);

		self.updateSummonerInfo = function() {
			$.ajax({
				url: "/summoners/forChannel/" + self.channel().Channel.name + ".json",
				dataType: "json",
				success: function(response) {
					self.summoners(response.summoners);
				}
			});
		};

		//rate limit chat resize
		self.windowWidth = ko.observable($(window).width()).extend({rateLimit: 500});
		self.windowHeight = ko.observable($(window).height()).extend({rateLimit: 500});

		$(window).resize(function() {
			self.windowWidth($(window).width());
			self.windowHeight($(window).height());
		});
		self.windowWidth.subscribe(function(newWindowWidth) {
			if(!self.isLoading()) {
				$("#chat-embed").width($("#chat-embed").parent().innerWidth());
			}
		});
		self.windowHeight.subscribe(function(newWindowHeight) {
			if(!self.isLoading()) {
				$("#chat-embed").height(newWindowHeight - $("#navbar").outerHeight());
			}
		});

		self.preventBubble =  function(e) {
			e = e || window.event;
			e.cancelBubble = true;
			if (e.stopPropagation) e.stopPropagation();
			if (e.preventDefault) e.preventDefault();
		};

		self.update(function() {
			self.updateSummonerInfo();
		});


	}
	ko.applyBindings(new ChannelsViewModel());
});
