require(["jquery", "bootstrap", "domReady!"], function($) {
	$("body").scrollspy({ target: "#faq-sidebar" })
	
	var sidebar = $("#faq-sidebar");
	var initialOffset = sidebar.offset().top;
	$(window).scroll(function() {
		var windowTop = $(window).scrollTop();
		if (initialOffset < windowTop) {
			sidebar.css({ width: sidebar.width() }).addClass("sticky-scrolling");
		} else {
			sidebar.removeClass("sticky-scrolling");
		}
	});
});