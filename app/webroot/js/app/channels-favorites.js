require(["jquery", "knockout", "knockout.localStorage", "domReady!"], function($, ko) {
	ko.components.register("channel-list", { require: "components/channel-list" });
	
	function FavoritesModelView() {
		self.favorites = ko.observableArray([]).extend({ localStorage: "favoriteChannels" });
		
		self.isLoading = ko.observable(false);
		self.favoriteChannels = ko.observableArray([]);
		
		self.clearFavorites = function() {
			self.favorites([]); // :(
			self.favoriteChannels([]); // it looks weird if there are still channels
		};
		
		self.update = function(callback) {
		
			self.isLoading(true);
			$.ajax({
				url: "/channels/favorites.json",
				dataType: "json",
				type: "POST",
				data: { data: self.favorites() },
				success: function(re) {
					self.favoriteChannels(re.channels);
					if(callback !== undefined) callback();
				},
				complete: function() { self.isLoading(false); }
			});
		
		};
		
		self.importUsername = ko.observable("");
		self.importStatus = ko.observable("");
		self.importStatusType = ko.observable("info");
		self.importFavorites = function() {
			var twitchUser = self.importUsername();
			self.importStatusType("info");
			self.importStatus("Getting channels followed by '"+twitchUser+"'...");
			$.ajax({
				url: "https://api.twitch.tv/kraken//users/"+escape(twitchUser)+"/follows/channels?limit=100",
				dataType: "jsonp",
				success: function(response) {
				
					if(response.error !== undefined) {
						self.importStatusType("danger");
						self.importStatus("Error getting followers: " +  response.message);
						return;
					}
				
					var importCount = 0;
					for(var i = 0; i < response.follows.length; ++i) {
						var channelName = response.follows[i].channel.name;
						if(self.favorites.indexOf(channelName) < 0) {
							self.favorites.push(channelName);
							importCount += 1;
						}
					}
					self.importStatus("Imported "+importCount+" channels, updating channel list...");
					self.update(function() {
						self.importStatusType("success");
						self.importStatus("Success! Added  " + importCount + " additional channels.");
						$("#importTwitchDialog").modal("hide");
					});
				},
				failure: function() {
					self.importStatusType("danger");
					self.importStatus("Couldn't get followers for '"+twitchUser+"'");
				}
			});
		};
		
		self.update();
	} 
	
	ko.applyBindings(new FavoritesModelView());
});