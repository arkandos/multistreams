require(["jquery", "knockout", "domReady!"], function($, ko) {

	var LOLKING_URL_SCHEME = /^http:\/\/www.lolking.net\/summoner\/(na|euw|eune|kr|br|tr|ru|lan|las|oce)\/([0-9]+)(#.+)?$/;

	function  RequestSummonerViewModel() {
		var self = this;

		self.regions = [
			{ title: "North America", region: "na" },
			{ title: "Europe West", region: "euw" },
			{ title: "Europe Nordic & East", region: "eune" },
			{ title: "Korea", region: "kr" },
			{ title: "Brasil", region: "br" },
			{ title: "Turkey", region: "tr" },
			{ title: "Russia", region: "ru" },
			{ title: "Latin America North", region: "lan" },
			{ title: "Latin America South", region: "las" },
			{ title: "Oceania", region: "oce" }
		];

		self.channelName = ko.observable("");
		self.region = ko.observable(self.regions[0]);
		self.summonerName = ko.observable("");
		self.summonerId = ko.observable("");

		//summoner names and ids cancel each other out
		self.summonerName.subscribe(function(newValue) {
			if(newValue.length > 0) {
				self.summonerId("");
				self.summonerLolking("");
			}
		});
		self.summonerId.subscribe(function(newValue) {
			if(newValue.length > 0) {
				self.summonerName("");
			}
		});

		self.lolkingState = ko.observable("");
		self.summonerLolking = ko.observable("");
		self.summonerLolking.subscribe(function(newValue) {
			var result = LOLKING_URL_SCHEME.exec(newValue);
			if(result) {
				self.region(ko.utils.arrayFirst(self.regions, function(region) {
					return region.region == result[1];
				}));
				self.summonerId(result[2]);
				self.lolkingState("success");
			} else {
				self.summonerId("");

				if(newValue.length > 0) self.lolkingState("error");
				else self.lolkingState("");
			}
		});

		self.hasError = ko.pureComputed(function() {
			var channelName = self.channelName(),
				lolkingState = self.lolkingState(),
				summonerName = self.summonerName();

			if(channelName.length == 0) return true;
			if(summonerName.length == 0 && lolkingState != "success") return true;
			if(lolkingState.length == 0 && summonerName.length == 0) return true;

			return false;
		}, self);

	};

	ko.applyBindings(new RequestSummonerViewModel());

});
