require(["jquery", "knockout", "domReady!"], function($, ko) {
	
	ko.components.register("channel-list", { require: "components/channel-list" });

	function SearchChannelsModel() {
		var self = this;

		self.searchTerm = ko.observable("");
		self.throttledSearchTerm = self.searchTerm.extend({ rateLimit: { timeout: 500, method: "notifyWhenChangesStop" } });
		self.throttledSearchTerm.subscribe(function() {
			self.update();
		});
		
		self.isLoading = ko.observable(false);
		self.searchResults = ko.observableArray([]);
		
		self.update = function() {
			if(self.isLoading()) return;
		
			self.searchResults([]);
			self.isLoading(true);
			
			$.ajax({
				url: "/channels/search/" + escape(self.searchTerm()) + "/.json",
				dataType: "json",
				success: function(re) { self.searchResults(re.channels); },
				complete: function() { self.isLoading(false); }
			});
		};
	}
	
	ko.applyBindings(new SearchChannelsModel());
});