define(["knockout"], function(ko) {
	ko.extenders.localStorage = function(target, localStorageKey) {

		if(!localStorageKey) return target;

		var initialValue = null;
		if(localStorage.hasOwnProperty(localStorageKey)) {
			try {
				initialValue = JSON.parse(localStorage.getItem(localStorageKey));
			} catch(e) {}
		}
		
		if(initialValue !== null) {
			target(initialValue);
		}

		target.subscribe(function(newValue) {
			localStorage.setItem(localStorageKey, ko.toJSON(newValue));
		});
		
		return target;
	};
});