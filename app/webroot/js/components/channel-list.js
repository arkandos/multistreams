define(["knockout", "text!./channel-list.html", "bootstrap", "knockout-bootstrap", "knockout.localStorage"], function(ko, templateHtml) {

	function ChannelListViewModel(params) {
		var self = this;
		params = params || {};

		self.tierNames = [ "Unranked", "Bronze", "Silver", "Gold", "Platinum", "Diamond", "Master", "Challenger" ];
		self.divisionNames = [ "??", "I", "II", "III", "IV", "V" ];

		self.channels = params.channels;
		self.currentPage = params.currentPage || ko.observable(0); // has to be observable
		self.channelsPerPage = params.channelsPerPage || 20;
		self.favorites = params.favorites || ko.observableArray([]).extend({ localStorage: "favoriteChannels" });

		self.toggleFavorite = function(favorite, event) {
			if(self.favorites.indexOf(favorite.Channel.name) >= 0) {
				self.favorites.remove(favorite.Channel.name);
			} else {
				self.favorites.push(favorite.Channel.name);
			}
			event.preventDefault();
		};

		self.pagesCount = ko.pureComputed(function() {
			return Math.ceil(ko.unwrap(self.channels).length / ko.unwrap(self.channelsPerPage));
		}, self);

		self.currentPageChannels = ko.pureComputed(function() {
			var currentPageChannels = [],
				channels = ko.unwrap(self.channels),
				currentPage = self.currentPage(),
				channelsPerPage = ko.unwrap(self.channelsPerPage);

			for(var i = currentPage * channelsPerPage; i < currentPage * channelsPerPage + channelsPerPage && i < channels.length; ++i) {
				currentPageChannels.push(channels[ i ]);
			}
			return currentPageChannels;
		}, self);

		self.nextPage = function() {
			var current = self.currentPage(), pagesCount = self.pagesCount();
			if(current + 1 < pagesCount) self.currentPage(current + 1);
		};
		self.previousPage = function() {
			var current = self.currentPage();
			if(current > 0) self.currentPage(current - 1);
		};

	};

	return { viewModel: ChannelListViewModel, template: templateHtml  };
});
