define(["knockout", "text!./masteries.html", "bootstrap"], function(ko, templateHtml) {

	var ALL_MASTERIES = {
		"4111": 1, "4112": 4, "4113": 4, "4114": 1,    "4211": 2, "4212": 2, "4213": 2, "4214": 2,    "4311": 1, "4312": 3, "4313": 3, "4314": 1,
		"4121": 1, "4122": 3, "4123": 3, "4124": 1,    "4221": 1, "4222": 3,            "4224": 1,               "4322": 3, "4323": 1, "4324": 1,
		"4131": 1, "4132": 1, "4133": 1, "4134": 3,    "4231": 1, "4232": 1, "4233": 3, "4234": 3,    "4331": 3, "4332": 1, "4333": 3, "4334": 1,
		"4141": 1, "4142": 3, "4143": 3, "4144": 1,    "4241": 3, "4242": 1, "4243": 1, "4244": 1,    "4341": 1, "4342": 1, "4343": 3, "4344": 2,
		"4151": 1, "4152": 3,            "4154": 1,    "4251": 1, "4252": 4, "4253": 1,                          "4352": 1, "4353": 3,
		           "4162": 1,                                     "4262": 1,                                     "4362": 1
	};

	function MasteriesViewModel(params) {
		var self = this;
		self.masteries = params.masteries;

		self.getMasteryName = function(tree, row, col) {
			return "4"+ko.unwrap(tree)+""+ko.unwrap(row)+""+ko.unwrap(col);
		};

		self.getMasteryRank = function(tree, row, col) {
			var name = self.getMasteryName(tree, row, col);
			var masteries = ko.unwrap(self.masteries).masteries;
			return masteries.hasOwnProperty(name) ? masteries[name] : 0;
		};

		self.getMaxRank = function(tree, row, col) {
			var name = self.getMasteryName(tree, row, col);
			return ALL_MASTERIES[ name ];
		};

		self.getTotalPoints = function(tree) {
			var totalPoints = 0,
				masteries = ko.unwrap(self.masteries).masteries;
			tree = ko.unwrap(tree);
			for(var name in masteries) {
				if(name.charAt(1) == ""+tree) {
					totalPoints += masteries[name];
				}
			}
			return totalPoints;
		};

		self.getMasteryImgUrl = function(tree, row, col) {
			var name = self.getMasteryName(tree, row, col);
			return "/img/masteries/"+name+".png";
		};

		self.isValidMastery = function(tree, row, col) {
			var name = self.getMasteryName(tree, row, col);
			return ALL_MASTERIES.hasOwnProperty(name);
		};
	}

	return { viewModel: MasteriesViewModel, template: templateHtml  };
});
