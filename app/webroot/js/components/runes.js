define(["knockout", "text!./runes.html", "text!./rune_data.json", "bootstrap"], function(ko, templateHtml, runeDataJson) {
	
	var RUNE_DATA = JSON.parse(runeDataJson);
	
	function RunesViewModel(params) {
		var self = this;
		
		self.runes = params.runes;
		self.runeSets = ko.pureComputed(function() {
			var runes = ko.unwrap(self.runes).runes,
				grouped = {},
				runeSets = [];
				
			for(var i = 0; i < runes.length; ++i) {
				if(!grouped.hasOwnProperty(runes[i])) {
					grouped[runes[i]] = 0;
				}
				grouped[runes[i]] += 1;
			}
			
			for(var runeId in grouped) if (grouped.hasOwnProperty(runeId)) {
				runeSets.push(ko.utils.extend({ id: runeId, number: grouped[runeId] }, RUNE_DATA[runeId]));
			}

			var TYPE_ORDER = ["black", "red", "yellow", "blue"];
			runeSets.sort(function(a, b) {
				var typeDiff = TYPE_ORDER.indexOf(a.type) - TYPE_ORDER.indexOf(b.type);
				return ((typeDiff != 0) ? typeDiff : (b.number - a.number));
			});
			
			
			return runeSets;
		}, self);
	};
	
	return { viewModel: RunesViewModel, template: templateHtml  };
});