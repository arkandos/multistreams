<?php

class Platform extends AppModel {
	public $primaryKey = "name";
	public $hasMany = array(
		/* "Channel" => array(
			"foreignKey" => "platform_name"
		), */
		"DataPath" => array(
			"foreignKey" => "platform_name"
		)
	);
	
	public function needsUpdate($platform) {
		$is_enabled = $platform["Platform"]["is_enabled"];
		$last_updated = strtotime($platform["Platform"]["last_updated"]);
		$update_interval = $platform["Platform"]["update_interval"];
		$now = time();
		
		return ($is_enabled && $last_updated + $update_interval < $now);
	}
}

?>
