<?php

App::uses("Hash", "Utility");

define("API_KEY", "fd4aac5a-1c70-4040-8297-9db884cf78d2");

class Summoner extends AppModel {
	public $belongsTo = array(
		"Channel" => array(
			"foreignKey" => "channel_name"
		)
	);

	public function afterFind($results, $primary = false) {
		foreach($results as &$result) if(isset($result["Summoner"])) {
			if(isset($result["Summoner"]["current_masteries"])) {
				$result["Summoner"]["current_masteries"] = json_decode($result["Summoner"]["current_masteries"], true);
			} if(isset($result["Summoner"]["current_runes"])) {
				$result["Summoner"]["current_runes"] = json_decode($result["Summoner"]["current_runes"], true);
			}
		}
		return $results;
	}
	public function beforeSave($options = array()) {
		if(isset($this->data["Summoner"]["current_masteries"])) {
			$this->data["Summoner"]["current_masteries"] = json_encode($this->data["Summoner"]["current_masteries"]);
		} if(isset($this->data["Summoner"]["current_runes"])) {
			$this->data["Summoner"]["current_runes"] = json_encode($this->data["Summoner"]["current_runes"]);
		}
	}

	public function needsUpdate($summoner) {
		$last_updated = strtotime($summoner["Summoner"]["last_updated"]);
		$update_interval = 300;
		$now = time();

		return ($last_updated + $update_interval < $now);
	}

	public function parseLeagueInfo(&$summoners, $data) {

		$leagues = array("UNRAKNED", "BRONZE", "SILVER", "GOLD", "PLATINUM", "DIAMOND", "MASTER", "CHALLENGER");
		$divisions = array("0", "I", "II", "III", "IV", "V");

		$json = json_decode($data, true);
		if(!$json) $json = array();

		foreach($summoners as &$summoner) {
			$id = $summoner["Summoner"]["id"];
			if(!array_key_exists($id, $json)) continue;

			foreach($json[$id] as $leagueEntry) if($leagueEntry["queue"] == "RANKED_SOLO_5x5") {
				$summoner["Summoner"]["tier_number"] = array_search($leagueEntry["tier"], $leagues);
				$summoner["Summoner"]["division"] = array_search($leagueEntry["entries"][0]["division"], $divisions);
				$summoner["Summoner"]["league_points"] = $leagueEntry["entries"][0]["leaguePoints"];
				break;
			}
		}
	}

	public function parseMasteries(&$summoners, $data) {
		$json = json_decode($data, true);
		if(!$json) $json = array();

		foreach($summoners as &$summoner) {
			$id = $summoner["Summoner"]["id"];
			if(!array_key_exists($id, $json)) continue;

			foreach($json[$id]["pages"] as $masteryPage) if($masteryPage["current"]) {
				$masteryPage["masteries"] = Hash::combine($masteryPage["masteries"], "{n}.id", "{n}.rank");
				$summoner["Summoner"]["current_masteries"] = $masteryPage;
				break;
			}
		}
	}

	public function parseRunes(&$summoners, $data) {
		$json = json_decode($data, true);
		if(!$json) $json = array();

		foreach($summoners as &$summoner) {
			$id = $summoner["Summoner"]["id"];
			if(!array_key_exists($id, $json)) continue;

			foreach($json[$id]["pages"] as $runePage) if($runePage["current"]) {
				$runePage["runes"] = Hash::sort($runePage["slots"], "{n}.runeSlotId", "asc", "numeric");
				$runePage["runes"] = Hash::extract($runePage["runes"], "{n}.runeId");
				unset($runePage["slots"]);
				$summoner["Summoner"]["current_runes"] = $runePage;
				break;
			}
		}
	}

	public function getSummonerNames($region, $ids) {
		$url = "https://".$region.".api.pvp.net/api/lol/".$region."/v1.4/summoner/"
			.implode(",", $ids)
			."?api_key=".API_KEY;

		$response = @file_get_contents($url);
		$json = json_decode($response, true);
		if(!$json) $json = array();

		$names = array();
		foreach($ids as $id) {
			array_push($names, $json[$id]["name"]);
		}
		return $names;
	}

	public function getSummonerIds($region, $names) {
		$url = "https://".$region.".api.pvp.net/api/lol/".$region."/v1.4/summoner/by-name/"
			.rawurlencode(implode(",", $names))
			."?api_key=".API_KEY;

		$response = @file_get_contents($url);
		$json = json_decode($response, true);
		if(!$json) $json = array();

		$ids = array();
		foreach($json as $internalName => $summoner) {
			$index = array_search($summoner["name"], $names);
			$ids[$index] = $summoner["id"];
		}
		return $ids;
	}

	public function validateSummoner(&$summoner) {
		if(!$this->Channel->exists($summoner["Summoner"]["channel_name"])) {
			return "Channel '". $summoner["Summoner"]["channel_name"] . "' not found";
		}

		if(!$summoner["Summoner"]["id"] && $summoner["Summoner"]["name"]) {
			$ids = $this->getSummonerIds($summoner["Summoner"]["region"], array( $summoner["Summoner"]["name"] ));

			if(!isset($ids[0])) {
				return "Summoner '" . $summoner["Summoner"]["name"] . "' not found";
			}

			$summoner["Summoner"]["id"] = $ids[0];
		} else if(!$summoner["Summoner"]["name"] && $summoner["Summoner"]["id"]) {
			$names = $this->getSummonerNames($summoner["Summoner"]["region"], array( $summoner["Summoner"]["id"] ));

			if(!isset($names[0]))  {
				return ("Summoner " . $summoner["Summoner"]["id"] . " not found");
			}

			$summoner["Summoner"]["name"] = $names[0];
		} else {
			return ("Incomplete Summoner data");
		}

		return null; //everythings ok
	}

	public function updateSummoners(&$summoners) {

		$summonersByRegion = array();
		foreach($summoners as &$summoner) {
			$region = $summoner["Summoner"]["region"];
			if(!array_key_exists($region, $summonersByRegion)) {
				$summonersByRegion[ $region ] = "" . $summoner["Summoner"]["id"];
			} else {
				$summonersByRegion[$region] .= "," . $summoner["Summoner"]["id"];
			}
		}

		$multi = curl_multi_init();
		$curl_handles = array("LeagueInfo" => array(), "Masteries" => array(), "Runes" => array());
		foreach($summonersByRegion as $region => $summonersForRegion) {

			$leagueUrl = "https://".$region.".api.pvp.net/api/lol/".$region."/v2.5/league/by-summoner/".$summonersForRegion."/entry?api_key=".API_KEY;

			$curl_handles["LeagueInfo"][$region] = curl_init();
			curl_setopt($curl_handles["LeagueInfo"][$region], CURLOPT_URL, $leagueUrl);
			curl_setopt($curl_handles["LeagueInfo"][$region], CURLOPT_RETURNTRANSFER, 1);
			curl_multi_add_handle($multi, $curl_handles["LeagueInfo"][$region]);

			$summonerBase = "https://".$region.".api.pvp.net/api/lol/".$region."/v1.4/summoner/".$summonersForRegion;

			$curl_handles["Masteries"][$region] = curl_init();
			curl_setopt($curl_handles["Masteries"][$region], CURLOPT_URL, $summonerBase . "/masteries?api_key=".API_KEY);
			curl_setopt($curl_handles["Masteries"][$region], CURLOPT_RETURNTRANSFER, 1);
			curl_multi_add_handle($multi, $curl_handles["Masteries"][$region]);

			$curl_handles["Runes"][$region] = curl_init();
			curl_setopt($curl_handles["Runes"][$region], CURLOPT_URL, $summonerBase . "/runes?api_key=".API_KEY);
			curl_setopt($curl_handles["Runes"][$region], CURLOPT_RETURNTRANSFER, 1);
			curl_multi_add_handle($multi, $curl_handles["Runes"][$region]);
		}

		// Start performing the request
		$running = NULL;
		do {
			while (CURLM_CALL_MULTI_PERFORM === curl_multi_exec($multi, $running)) {};
			if (!$running) break;
			while (curl_multi_select($multi) === 0) {};
		} while (true);


		foreach($curl_handles as $dataType => $handles) {
			$callback = array($this, "parse".$dataType);
			foreach($handles as $region => $ch) {
				$err = curl_error($ch);
				if($err == "") {
					$data = curl_multi_getcontent($ch);
					call_user_func($callback, $summoners, $data);
				}
				curl_multi_remove_handle($multi, $ch);
				curl_close($ch);
			}
		}

		return $summoners;
	}

}

?>
