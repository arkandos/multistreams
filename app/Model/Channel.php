<?php

App::uses("Hash", "Utility");

class Channel extends AppModel {
	public $actsAs = array("Containable");

	public $primaryKey = "name";
	public $belongsTo = array("Platform" => array(
		"foreignKey" => "platform_name"
	));
	public $hasMany = array("Summoner" => array(
		"foreignKey" => "channel_name",
		"conditions" => array("Summoner.confirmed" => 1)
	));
	public $hasOne = array("MainSummoner" => array(
		"className" => "Summoner",
		"foreignKey" => "channel_name",
		//i dont know if i can show that anyone, but it works.
		"conditions" => array("MainSummoner.id = (SELECT MainSummoner.id FROM summoners AS MainSummoner WHERE MainSummoner.channel_name = Channel.name AND MainSummoner.confirmed = 1 ORDER BY MainSummoner.tier_number DESC, MainSummoner.division ASC, MainSummoner.league_points DESC LIMIT 1)"),
	));

	public function parseChannels($platform, $data) {
		$json = json_decode($data, true);

		//rearrange for easy access
		$dataPaths = Hash::combine($platform["DataPath"], "{n}.row_name", "{n}");

		// no point in updating if we don't have any streams
		if(!isset($dataPaths["streams"])) return;

		$channelsData = array();
		$rows = array("name", "url", "embed_video_url", "embed_chat_url", "display_name", "title", "description", "is_live", "is_hidden", "profile_logo_url", "preview_url", "followers_count", "viewers_count");
		$streams = Hash::get($json, $dataPaths["streams"]["path"]);
		foreach($streams as $stream) {
			$channelData = array("platform_name" => $platform["Platform"]["name"], "is_live" => 1, "is_hidden" => 0);
			foreach($rows as $row) {
				if(isset($dataPaths[$row])) {
					$channelData[$row] = $dataPaths[$row]["prefix"]
						. Hash::get($stream, $dataPaths[$row]["path"])
						. $dataPaths[$row]["postfix"];
				}
			}

			//BUG: twitch (??) sometimes sends us invalid stream records
			//only actually update this channel if we got a url for it
			if($channelData["url"]) {
				array_push($channelsData, array("Channel" => $channelData));
			}
		}

		//relative view count
		$totalViewers = Hash::reduce($channelsData, "{n}.Channel.viewers_count", function($a, $b) { return $a+$b; });
		if($totalViewers == 0) $totalViewers = 1;

		for($i = 0; $i < count($channelsData); ++$i) {
			//added * log to give twitch a chance
			// - 1 viewer to ignore 1 viewer streams
				$channelsData[$i]["Channel"]["viewers_relative"] =
					(float)($channelsData[$i]["Channel"]["viewers_count"] - 1) / $totalViewers * log($totalViewers);
		}

		/* $this->deleteAll(array(
			"Channel.platform_name" => $platform["Platform"]["name"]
		));*/
		$this->updateAll(
			array("Channel.is_live" => 0),
			array("Channel.platform_name" => $platform["Platform"]["name"])
		);
		if(count($channelsData) > 0) {
			$this->saveMany($channelsData);
		}

	}

	public function updateChannels($platform) {
		if(!$platform["Platform"]["is_enabled"]) return;

		$api_list_top = $platform["Platform"]["api_base"] . $platform["Platform"]["api_list_top"];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api_list_top);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		curl_close($ch);

		$this->parseChannels($platform, $data);
	}

	public function updateAllChannels($platforms) {
		//foreach($platforms as $platform) $this->updateChannels($platform);
		$multi = curl_multi_init();
		$curl_handles = array();
		foreach($platforms as $i => $platform) {
			$api_list_top = $platform["Platform"]["api_base"] . $platform["Platform"]["api_list_top"];
			$curl_handles[$i] = curl_init();
			curl_setopt($curl_handles[$i], CURLOPT_URL, $api_list_top);
			curl_setopt($curl_handles[$i], CURLOPT_RETURNTRANSFER, 1);
			curl_multi_add_handle($multi, $curl_handles[$i]);
		}

		// Start performing the request
		$running = NULL;
		do {
			while (CURLM_CALL_MULTI_PERFORM === curl_multi_exec($multi, $running)) {};
			if (!$running) break;
			while (curl_multi_select($multi) === 0) {};
		} while (true);

		foreach($curl_handles as $i => $ch) {
			$err = curl_error($ch);
			if($err == "") {
				$data = curl_multi_getcontent($ch);
				$this->parseChannels($platforms[$i], $data);
			}

			curl_multi_remove_handle($multi, $ch);
			curl_close($ch);
		}

		curl_multi_close($multi);
		return true;
	}
}

?>
