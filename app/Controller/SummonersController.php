<?php

class SummonersController extends AppController {


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow("forChannel", "request_summoner");
	}

	public function add() {
		if($this->request->is("post")) {
			$summoner = $this->request->data;

			$error = $this->Summoner->validateSummoner($summoner);
			if($error) {
				$this->Session->setFlash(htmlspecialchars($error));
				return;
			}

			$summonerArray = array(&$summoner);
			$this->Summoner->updateSummoners($summonerArray);
			$summoner["Summoner"]["last_updated"] = date("Y-m-d H:i:s");
			$summoner["Summoner"]["confirmed"] = 1;

			$this->Summoner->create();
			if ($this->Summoner->save($summoner)) {
                $this->Session->setFlash("Created new summoner '".$summoner["Summoner"]["name"]."' (".$summoner["Summoner"]["id"].")");
                //return $this->redirect(array('action' => 'index'));
            } else {
				$this->Session->setFlash("Couldn't create this summoner");
			}
		}

		$this->request->data = array();
	}

	public function request_summoner() {
		if($this->request->is("post")) {
			$summoner = $this->request->data;

			$error = $this->Summoner->validateSummoner($summoner);
			if($error) {
				$this->Session->setFlash(htmlspecialchars($error));
				return;
			}

			$this->Summoner->id = $summoner["Summoner"]["id"];

			if(!$this->Summoner->exists()) {
				$this->Summoner->create();
				$summoner["Summoner"]["requests"] = 1;
				$summoner["Summoner"]["confirmed"] = 0;
			} else {
				$summoner["Summoner"]["requests"] = $this->Summoner->field("requests") + 1;
				$summoner["Summoner"]["confirmed"] = $this->Summoner->field("confirmed");
			}

			if($this->Summoner->save($summoner)) {
				$this->Session->setFlash("Requested summoner '".$summoner["Summoner"]["name"]."' (".$summoner["Summoner"]["id"].") for " . $summoner["Summoner"]["channel_name"]);
			} else {
				$this->Session->setFlash("Couldn't request summoner");
			}
		}

		$this->request->data = array();
	}

	public function confirm_requests() {
		if($this->request->is(array("put", "post"))) {

			$summoner = $this->Summoner->findById($this->request->data["Summoner"]["id"]);
			if(!$summoner) {
				throw new NotFoundException();
			}

			if($this->request->data["Summoner"]["confirmed"] == 1) {
				//this is also copypaste from add
				$summonerArray = array(&$summoner);
				$this->Summoner->updateSummoners($summonerArray);
				$summoner["Summoner"]["last_updated"] = date("Y-m-d H:i:s");
				$summoner["Summoner"]["confirmed"] = 1;

				if($this->Summoner->save($summoner)) {
					$this->Session->setFlash("Confirmed summoner '".$summoner["Summoner"]["name"]."' (".$summoner["Summoner"]["id"].") for " . $summoner["Summoner"]["channel_name"]);
				} else {
					$this->Session->setFlash("Couldn't confirm summoner");
				}

			} else {

				if($this->Summoner->delete($this->request->data["Summoner"]["id"])) {
					$this->Session->setFlash("Declined summoner '".$summoner["Summoner"]["name"]."' (".$summoner["Summoner"]["id"].") for " . $summoner["Summoner"]["channel_name"]);
				} else {
					$this->Session->setFlash("Couldn't decline summoner");
				}

			}
		}

		$this->request->data =  $this->Summoner->find("first", array(
			"conditions" => array("Summoner.confirmed" => 0),
			"order" => array("Summoner.requests DESC"),
			"recursive" => 0,
		));

		if(!$this->request->data) {
			$this->flash("No more summoners need confirmation!", "/");
		}

		unset($this->request->data["Summoner"]["confirmed"]);

		$this->set("request", $this->request->data);
	}

	public function forChannel($channelName) {
		if(!$channelName) throw new NotFoundException();

		//only json requests allowed
		if(!isset($this->request->params["ext"]) || $this->request->params["ext"] != "json") {
			throw new BadRequestException();
		}

		$summoners = $this->Summoner->find("all", array(
			"conditions" => array("Summoner.channel_name" => $channelName, "Summoner.confirmed" => 1),
			"order" => array("Summoner.region", "Summoner.tier_number DESC", "Summoner.division ASC", "Summoner.league_points DESC"),
			"recursive" => -1
		));

		//check for updates
		$summonersToUpdate = array();
		foreach($summoners as &$summoner) {
			if($this->Summoner->needsUpdate($summoner)) {
				$this->Summoner->save(array(
					"id" => $summoner["Summoner"]["id"],
					"last_updated" => date("Y-m-d H:i:s")
				));
				unset($summoner["Summoner"]["created"]);
				unset($summoner["Summoner"]["modified"]);
				unset($summoner["Summoner"]["last_updated"]);
				$summonersToUpdate[] = &$summoner;
			}
		}

		if(count($summonersToUpdate) > 0) {
			$this->Summoner->updateSummoners($summonersToUpdate);
			$this->Summoner->saveMany($summonersToUpdate);
		}
		$this->set("summoners", $summoners);
		$this->layout = "ajax";
	}

}

?>
