<?php

App::uses('Controller', 'Controller');
class AppController extends Controller {
	public $components = array(
		"Session",
		"Auth" => array(
			"loginRedirect" => "/",
			"logoutRedirect" => "/",
			"authenticate" => array(
				"Form" => array(
					"passwordHasher" => "Blowfish"
				)
			),
			"authorize" => array("Controller"),
		),
	);
	public $helpers = array(
		'Html' => array('className' => 'MyHtml'),
		"Form",
		"Session",
	);

	public function beforeRender() {
		parent::beforeRender();
		$this->set("loggedIn", $this->Auth->loggedIn());
		$this->set("user", $this->Auth->user());
	}

	public function isAuthorized($user) {

		if(!isset($user["role"]) || $user["role"] === "pending") {
			return false;
		} else if($user["role"] === "admin") {
			return true;
		}

		return false;
	}

}
