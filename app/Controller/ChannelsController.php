<?php

class ChannelsController extends AppController {
	public $uses = array("Channel", "Platform");
	public $components = array('RequestHandler');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();

		if(isset($this->request->params["ext"]) && $this->request->params["ext"] == "json") {
			//update channels
			$platforms = $this->Platform->find("all");
			$platformsToUpdate = array();

			foreach($platforms as $platform) {
				if($this->Platform->needsUpdate($platform)) {
					$this->Platform->save(array(
						"name" => $platform["Platform"]["name"],
						"last_updated" => date("Y-m-d H:i:s")
					));
					array_push($platformsToUpdate, $platform);
				}
			}
			$this->Channel->updateAllChannels($platformsToUpdate);
		}
	}

	public function index() {
		$platforms = $this->Platform->find("all");
		$topChannels = array();
		foreach($platforms as $platform) {
			$topChannel = $this->Channel->find("first", array(
				"conditions" => array("platform_name" => $platform["Platform"]["name"], "is_live" => 1, "is_hidden" => 0),
				"order" => array("Channel.viewers_count DESC"),
				"recursive" => -1
			));

			if($topChannel) {
				$topChannel["Platform"] = $platform["Platform"];
				array_push($topChannels, $topChannel);
			}
		}
		$this->set("channels", $topChannels);
	}

	public function live() {
		//the hard work is done after the html is loaded,
		//it looks faster that way
		if(isset($this->request->params["ext"]) && $this->request->params["ext"] == "json") {

			$options = array(
				"conditions" => array("is_live" => 1, "is_hidden" => 0),
				"limit" => 20,
				"offset" => 0,
				"order" => array("Channel.viewers_relative DESC"),
				"contain" => "MainSummoner",
			);

			if(isset($this->request->named["page"])) {
				$options["offset"] = $this->request->named["page"] * $options["limit"];
			}

			if(isset($this->request->named["sort"])) switch($this->request->named["sort"]) {
				case "viewers":
					$options["order"] = array("Channel.viewers_count DESC");
					break;
				case "relative":
					$options["order"] = array("Channel.viewers_relative DESC");
					break;
				case "followers":
					$options["order"] = array("Channel.followers_count DESC");
					break;
				case "elo":
					$options["order"] = array("MainSummoner.tier_number DESC", "MainSummoner.division ASC", "MainSummoner.league_points DESC", "Channel.viewers_count DESC");
					break;
				default: break;
			}


			$this->set("channels", $this->Channel->find("all", $options));
			$this->set("pages_count", (int)ceil($this->Channel->find("count", array(
				"conditions" => array("is_live" => 1, "is_hidden" => 0)
			)) / 20.0));

			$this->set("_serialize", array("channels", "pages_count"));
		}
	}

	public function watch($channelName) {
		if(!$channelName) throw NotFoundException();

		$channel = $this->Channel->find("first", array(
			"conditions" => array("Channel.name" => $channelName),
			"recursive" => -1
		));

		if(!$channel) throw NotFoundException();

		$this->set("channel", $channel);
		$this->set("_serialize", array("channel"));
	}

	public function search($query = "") {
		if(isset($this->request->params["ext"]) && $this->request->params["ext"] == "json") {
			if(!$query) throw NotFoundException();

			$channels = $this->Channel->find("all", array(
				"conditions" => array(
					"Channel.is_live" => 1,
					"Channel.is_hidden" => 0,
					"OR" => array(
						"Channel.name LIKE" => "%" . $query . "%",
						"Channel.title LIKE" => "%" . $query . "%"
					)
				),
				"order" => array("Channel.viewers_count DESC"),
				"contain" => "MainSummoner"
			));

			$this->set("channels", $channels);
			$this->set("_serialize", array("channels"));
		} else {
			$this->set("searchTerm", $query);
		}
	}

	public function favorites() {
		if(isset($this->request->params["ext"]) && $this->request->params["ext"] == "json") {
			$favorites = $this->request->data;

			$channels = $this->Channel->find("all", array(
				"conditions" => array(
					"Channel.is_live" => 1,
					"Channel.name" => $favorites,
				),
				"order" => array("Channel.viewers_count DESC"),
				"contain" => "MainSummoner",
			));

			$this->set("channels", $channels);
			$this->set("_serialize", array("channels"));
		}
	}
}

?>
